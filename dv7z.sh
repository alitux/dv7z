#!/bin/bash
# dv7z compress a docker volume in a 7z 
# Usage: dv7z CONTAINER_NAME OUTPUT_DIR

if [ $# -ne 2 ];  then
    echo "Need 2 parameters to work." 
    echo "Usage: dv7z CONTAINER_NAME OUTPUT_DIR"
    exit 1
fi

OUTPUT_DIR=$2

for CONTAINER_NAME in $(docker ps -q -f name="$1"); do
    VOLUME_NAME=$(docker inspect --format='{{range .Mounts}}{{if eq .Type "volume"}}{{.Name}}{{end}}{{end}}' $CONTAINER_NAME)
    if [ -z "$VOLUME_NAME" ]; then
        echo "No volumes found for container $CONTAINER_NAME"
        continue
    fi
    for VOLUME_PATH in $(docker inspect --format='{{range .Mounts}}{{if eq .Type "volume"}}{{printf "%s\n" .Source}}{{end}}{{end}}' $CONTAINER_NAME); do
        OUTPUT_NAME=$(basename "$(dirname $VOLUME_PATH)")
        OUTPUT_FILE="$OUTPUT_NAME.7z"
        echo "Compress $VOLUME_PATH to $OUTPUT_DIR/$OUTPUT_FILE"
        7z a -r "$OUTPUT_DIR/$OUTPUT_FILE" "$VOLUME_PATH"
    done
done

